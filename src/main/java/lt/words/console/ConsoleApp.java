package lt.words.console;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

import com.google.common.collect.Lists;

import lt.words.app.config.AppOptions;
import lt.words.app.model.WordCount;
import lt.words.app.service.WordCountService;

@SpringBootApplication(scanBasePackageClasses = ConsoleApp.class)
@ComponentScan(basePackages = { "lt.words.app" })
public class ConsoleApp implements CommandLineRunner {
	@Autowired
	private WordCountService service;
	private static AppOptions options;

	public static void main(String[] args) {
		//@formatter:off
    	SpringApplication springApplication = 
                new SpringApplicationBuilder()
                .sources(ConsoleApp.class)
                .web(false)
                .build();
    	//@formatter:on
		options = new AppOptions();
		springApplication.run(args);
	}

	@Override
	public void run(String... arg0) throws IOException {
		try {
			List<Stream<String>> streams = readAllFiles();
			Map<String, Integer> result = service.calculateWords(streams);

			// initiate output array lists depending on letter range count
			List<List<WordCount>> wordGroups = new ArrayList<List<WordCount>>(options.getWordGroupRanges().size());
			for (int i = 0; i < options.getWordGroupRanges().size(); i++) {
				wordGroups.add(Lists.newArrayList());
			}

			// split results into corresponding word groups by ranges, specified in AppOptions
			for (Entry<String, Integer> entry : result.entrySet()) {
				for (int i = 0; i < options.getWordGroupRanges().size(); i++) {
					char lowerRange = options.getWordGroupRanges().get(i).getLowerRange();
					char upperRange = options.getWordGroupRanges().get(i).getUpperRange();
					if (entry.getKey().charAt(0) >= lowerRange && entry.getKey().charAt(0) <= upperRange) {
						wordGroups.get(i).add(new WordCount(entry.getKey(), entry.getValue()));
						if (!options.isOverlappingRanges()) { // if ranges are overlapping, loop should continue
							break;
						}
					}
				}
			}

			// write to files
			for (int i = 0; i < options.getWordGroupRanges().size(); i++) {
				writeResultGroup(Paths.get(String.format("%s[%s-%s].txt", options.getOutputDir(),
						options.getWordGroupRanges().get(i).getLowerRange(),
						options.getWordGroupRanges().get(i).getUpperRange())), wordGroups.get(i));
			}

		} catch (InterruptedException e) {
			System.err.println("Concurrency error while processing input data");
			e.printStackTrace();
		}
	}

	private List<Stream<String>> readAllFiles() {
		List<Stream<String>> streams = Lists.newArrayList();
		//@formatter:off
		// reads all files in specified directory
		try (Stream<Path> paths = Files.walk(Paths.get(options.getInputDir()))) {
			paths
				.filter(Files::isRegularFile)
				.forEach((p) -> {
					try {
						streams.add(Files.lines(p));
					} catch (IOException e) {
						System.err.println("Error while reading lines from file " + p.getFileName());
						e.printStackTrace();
					}
				});
		} catch (IOException e1) {
			System.err.println("Error while reading files from input directory: " + options.getInputDir());
			e1.printStackTrace();
		}
		//@formatter:on
		return streams;
	}

	private void writeResultGroup(Path path, List<WordCount> words) {
		try {
			Files.deleteIfExists(path);
		} catch (IOException e1) {
			System.err.println("Error while deleting already present output file " + path.getFileName());
			e1.printStackTrace();
		}
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
			for (WordCount count : words) {
				writer.write(count.toString());
				writer.append('\n'); // add new line for readability
			}
		} catch (IOException e) {
			System.err.println("Error while writing results to output directory: " + options.getOutputDir());
			e.printStackTrace();
		}
	}
}
