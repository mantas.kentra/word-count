package lt.words.app.model;

public class MutableInt {
	private Integer value = 1;

	public void increment() {
		++value;
	}

	public Integer getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value.toString();
	}
	
	public static Integer sum(MutableInt a, MutableInt b) {
        return a.getValue() + b.getValue();
    }
}
