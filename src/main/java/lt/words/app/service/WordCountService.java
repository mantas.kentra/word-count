package lt.words.app.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public interface WordCountService {

	public Map<String, Integer> calculateWords(List<Stream<String>> fileStreams) throws InterruptedException;
}
