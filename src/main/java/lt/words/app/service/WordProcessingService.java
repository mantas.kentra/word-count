package lt.words.app.service;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.stream.Stream;

import lt.words.app.model.MutableInt;

public interface WordProcessingService {

	public Collection<List<String>> splitByFirstLetter(Stream<String> fileContent) throws IOException;
	
	public Map<String, Integer> mergeWordMaps(List<Future<Map<String, MutableInt>>> wordMapsList);

}
