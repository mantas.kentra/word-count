package lt.words.app.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;

import lt.words.app.config.AppOptions;
import lt.words.app.model.MutableInt;
import lt.words.app.service.WordCountService;
import lt.words.app.service.WordProcessingService;

@Service
public class WordCountServiceImpl implements WordCountService {

	@Autowired
	private WordProcessingService processingService;
	private AppOptions options = new AppOptions();

	public Map<String, Integer> calculateWords(List<Stream<String>> fileStreams) throws InterruptedException{
		List<Callable<Map<String, MutableInt>>> tasks = new ArrayList<Callable<Map<String, MutableInt>>>();
		
		for (Stream<String> fileStream : fileStreams)
			tasks.add(calculateWordsFromSingleStream(fileStream));
		
		ExecutorService executor = Executors.newFixedThreadPool(tasks.size() > options.getThreadLimit() ? options.getThreadLimit() : tasks.size());
		List<Future<Map<String, MutableInt>>> results = executor.invokeAll(tasks);
		executor.shutdown();

		return processingService.mergeWordMaps(results);
	}
	
	private Callable<Map<String, MutableInt>> calculateWordsFromSingleStream(final Stream<String> fileContent) {
		return new Callable<Map<String, MutableInt>>() {
			public Map<String, MutableInt> call() throws IOException {
				return countWords(fileContent);
			}
		};
	}
	
	private Map<String, MutableInt> countWords(Stream<String> fileContent) throws IOException {
		Map<String, MutableInt> wordCountMap = Maps.newLinkedHashMap();
		// split by first letter to decrease loop cycle count
		for (List<String> wordsGroupedByLetter : processingService.splitByFirstLetter(fileContent)) {
			for (String word : wordsGroupedByLetter) {
				MutableInt count = wordCountMap.get(word);
				if (count == null) {
					wordCountMap.put(word, new MutableInt()); // MutableInt default value is 1
				} else {
					count.increment();
				}
			}
		}

		return wordCountMap;
	}
}
