package lt.words.app.service.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;

import lt.words.app.model.MutableInt;
import lt.words.app.service.WordProcessingService;

@Service
public class WordProcessingServiceImpl implements WordProcessingService {

	public Collection<List<String>> splitByFirstLetter(Stream<String> fileContent) throws IOException {
		//@formatter:off
		return fileContent
			    .flatMap(s -> Stream.of(s.split("[^a-zA-Z]"))) //pick only words
			    .filter(s -> s.length() > 0)
			    .map(s -> s.toLowerCase())
			    .collect(Collectors.groupingBy(s -> s.charAt(0)))
			    .values();
		//@formatter:off
	}
	
	public Map<String, Integer> mergeWordMaps(List<Future<Map<String, MutableInt>>> wordMapsList) {
		Map<String, Integer> mergedMap = Maps.newLinkedHashMap();
		//@formatter:off
		wordMapsList.forEach(map -> {
			try {
				map
				  .get()
				  .forEach((k, v) -> mergedMap.merge(k, v.getValue(), Integer::sum));
			} catch (InterruptedException | ExecutionException e) {
			}
		});
		
		// sort result before returning
		return mergedMap
				.entrySet()
				.stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		//@formatter:on
	}

}
