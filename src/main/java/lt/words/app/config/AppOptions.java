package lt.words.app.config;

import java.util.List;

import com.google.common.collect.Lists;

public class AppOptions {

	private final List<LetterRange> wordGroupRanges = Lists.newArrayList();
	private final String inputDir;
	private final String outputDir;
	private final boolean overlappingRanges;
	private final int threadLimit;

	public AppOptions() {
		this.wordGroupRanges.add(new LetterRange('a', 'g'));
		this.wordGroupRanges.add(new LetterRange('h', 'n'));
		this.wordGroupRanges.add(new LetterRange('o', 'u'));
		this.wordGroupRanges.add(new LetterRange('v', 'z'));
		this.overlappingRanges = false;
		
		this.inputDir = "src/main/resources/input/";
		this.outputDir = "src/main/resources/output/";
		
		this.threadLimit = 4;
	}

	public List<LetterRange> getWordGroupRanges() {
		return wordGroupRanges;
	}

	public String getInputDir() {
		return inputDir;
	}

	public String getOutputDir() {
		return outputDir;
	}

	public boolean isOverlappingRanges() {
		return overlappingRanges;
	}

	public int getThreadLimit() {
		return threadLimit;
	}
	
}
