package lt.words.app.config;

public class LetterRange {
	private char lowerRange;
	private char upperRange;

	public LetterRange(char starts, char ends) {
		this.lowerRange = starts;
		this.upperRange = ends;
	}

	public char getLowerRange() {
		return lowerRange;
	}

	public void setLowerRange(char lowerRange) {
		this.lowerRange = lowerRange;
	}

	public char getUpperRange() {
		return upperRange;
	}

	public void setUpperRange(char upperRange) {
		this.upperRange = upperRange;
	}

}