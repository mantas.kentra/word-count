package lt.words.app.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.common.collect.Lists;

import lt.words.app.config.AppOptions;
import lt.words.app.model.WordCount;
import lt.words.app.service.WordCountService;

@RestController
public class WordCountController {

	@Autowired
	private WordCountService fileProcessingService;
	private List<Stream<String>> fileStreams = Lists.newArrayList();
	private AppOptions options = new AppOptions();

	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
	public void fileUpload(HttpServletRequest request, HttpServletResponse response) {
		fileStreams.clear(); // clear previously uploaded file contents
		MultipartHttpServletRequest mRequest;
		try {
			mRequest = (MultipartHttpServletRequest) request;
			List<MultipartFile> files = mRequest.getFiles("files");
			for (MultipartFile mFile : files) {
				InputStream in = mFile.getInputStream();
				fileStreams.add(new BufferedReader(new InputStreamReader(in)).lines());
			}

		} catch (IOException e) {
			System.err.println("Error while reading uploaded file");
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/wordCount", method = RequestMethod.POST, produces = "application/json")
	public List<List<WordCount>> getCount() {
		List<List<WordCount>> wordGroups = Lists.newArrayList();
		try {
			Map<String, Integer> wordCountMap = fileProcessingService.calculateWords(fileStreams);

			// initiate output array lists depending on letter range count
			wordGroups = new ArrayList<List<WordCount>>(options.getWordGroupRanges().size());
			for (int i = 0; i < options.getWordGroupRanges().size(); i++) {
				wordGroups.add(Lists.newArrayList());
			}

			// split wordCountMap into corresponding word groups by ranges, specified in AppOptions
			for (Entry<String, Integer> entry : wordCountMap.entrySet()) {
				for (int i = 0; i < options.getWordGroupRanges().size(); i++) {
					
					char lowerRange = options.getWordGroupRanges().get(i).getLowerRange();
					char upperRange = options.getWordGroupRanges().get(i).getUpperRange();
					
					if (entry.getKey().charAt(0) >= lowerRange && entry.getKey().charAt(0) <= upperRange) {
						wordGroups.get(i).add(new WordCount(entry.getKey(), entry.getValue()));
						// if ranges are overlapping, loop should continue
						if (!options.isOverlappingRanges()) { 
							break;
						}
					}
				}
			}
		} catch (InterruptedException e) {
			System.err.println("Concurrency error while processing input data");
			e.printStackTrace();
		}

		return wordGroups;
	}

}
