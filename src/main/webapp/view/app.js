var app = angular.module('app', ['ui.grid', 'ui.grid.pagination']);

app.controller('WordCountCtrl', ['$scope', '$http', function($scope, $http) {

 $scope.disableCalc = true;
	
 $scope.uploadFiles = function(e) {
  $scope.disableCalc = false;
  var data = new FormData();
  for (var i = 0; i < e.files.length; i++) {
   data.append("files", e.files[i]);
  }
  $http.post("/fileUpload", data, {
    transformRequest: angular.identity,
    headers: {
     'Content-Type': undefined
    }
   });
 };

 $scope.getWordCount = function(callback) {
  $scope.disableCalc = true;
  $scope.gridsVisible = true;
  $http({
   method: 'POST',
   url: 'wordCount'
  }).success(function(result) {
   $scope.gridAtoG.data = result[0];
   $scope.gridHtoN.data = result[1];
   $scope.gridOtoU.data = result[2];
   $scope.gridVtoZ.data = result[3];
  });
 }

 $scope.gridAtoG = {
  enableColumnMenus: false,
  useExternalPagination: true,
  columnDefs: [{
   field: 'name',
   displayName: 'Word [A-G]'
  }, {
   field: 'count',
   displayName: 'Count'
  }]
 };

 $scope.gridHtoN = {
  enableColumnMenus: false,
  useExternalPagination: true,
  columnDefs: [{
   field: 'name',
   displayName: 'Word [H-N]'
  }, {
   field: 'count',
   displayName: 'Count'
  }]
 };

 $scope.gridOtoU = {
  enableColumnMenus: false,
  useExternalPagination: true,
  columnDefs: [{
   field: 'name',
   displayName: 'Word [O-U]'
  }, {
   field: 'count',
   displayName: 'Count'
  }]
 };

 $scope.gridVtoZ = {
  enableColumnMenus: false,
  useExternalPagination: true,
  columnDefs: [{
   field: 'name',
   displayName: 'Word [V-Z]'
  }, {
   field: 'count',
   displayName: 'Count'
  }]
 };

}]);